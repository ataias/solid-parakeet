//
//  User.swift
//  
//
//  Created by Ataias Pereira Reis on 29/06/22.
//

import Foundation
import Vapor

struct User: Content {
  var id = UUID()
  var email: String
  var firstName: String
  var lastName: String

  static func random() -> User {
    User(id: UUID(), email: faker.internet.email(), firstName: faker.name.firstName(), lastName: faker.name.lastName())
  }
}

struct UsersParams: Content {
  var quantity: Int
}

