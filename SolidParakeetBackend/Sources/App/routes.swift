import Vapor

func routes(_ app: Application) throws {
  app.get("user") { req -> User in
    return User.random()
  }

  app.get("users") { req -> [User] in
    let params = try req.query.decode(UsersParams.self)
    return (1...params.quantity).map { _ in User.random() }
  }

  app.get("product") { req -> Product in
    let params = try req.query.decode(ProductParams.self)
    return Product.random(params)
  }

  app.get("products") { req -> [Product] in
    let params = try req.query.decode(ProductParams.self)
    let quantity = params.quantity ?? 1
    return (1...quantity).map { _ in Product.random(params)}
  }
}
