//
//  Product.swift
//  
//
//  Created by Ataias Pereira Reis on 29/06/22.
//

import Foundation
import Vapor
import Fakery

struct Product: Content {
  var id = UUID()
  var name: String
  var description: String
  var price: Decimal

  static func random(_ params: ProductParams) -> Product {

    let paragraphCount = params.descriptionParagraphCount ?? 3
    let minPrice = params.minPrice ?? 100
    let maxPrice = params.maxPrice ?? 2000

    return Product(
      name: faker.commerce.productName(),
      description: faker.lorem.paragraphs(amount: paragraphCount),
      price: Decimal(Int.random(in: minPrice ... maxPrice))
    )
  }
}

struct ProductParams: Content {
  var quantity: Int?
  var descriptionParagraphCount: Int?
  var minPrice: Int?
  var maxPrice: Int?
}

