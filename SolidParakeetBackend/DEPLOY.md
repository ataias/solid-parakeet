# Deploy

## Ubuntu 22.04

I am having some issues in using `swift build`` on a Ubuntu 22.04 ARM machine. I
tried building my macos with a Linux image though. The official images do not
have a Ubuntu jammy version, but [swift-arm][1] does and that's what I ended up
using.

[1]: https://hub.docker.com/r/swiftarm/swift/tags

With docker we can then build:

```sh
cd backend
docker run --volume (PWD):/project --workdir /project swiftarm/swift:5.6.2-ubuntu-jammy swift build -c release
```

Afterwards, copying the binary and enabling the service with systemd works.
