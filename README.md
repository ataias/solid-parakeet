# Solid Parakeet

A React-Redux application that demonstrates basic interaction with IndexedDB.

The basic idea comes from the [First DB App][1] project idea. I took the idea to
help me reinforce what I had been learning/reviewing of React, Redux and
Material UI. It ended up going out the exact specs though, but it served my
original plan for it which was just reinforcing my learnings with a simple app.

[1]: https://github.com/florinpop17/app-ideas/blob/b47cf8261f001edef30217ac6021665b515cda46/Projects/1-Beginner/First-DB-App.md

A simple API was made to generate fake data and it is also part of this repo.

## Getting started

### Build

#### Front-End

```sh
cd SolidParakeetFrontend
pnpm install
pnpm run build
```

#### Backend

```sh
cd SolidParakeetBackend
swift build -c release
```

## Demo

Visit [solid-parakeet.case.dev.br](https://solid-parakeet.case.dev.br/)

## License

[MIT](./LICENSE)
