import "./index.css";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";

import { BrowserRouter, Route, Routes } from "react-router-dom";

import About from "./modules/about/About";
import App from "./App";
import Orders from "./modules/orders/Orders";
import ProductsItem from "./modules/products/ProductsItem";
import { Provider } from "react-redux";
import React from "react";
import ReactDOM from "react-dom/client";
import Users from "./modules/users/Users";
import UsersItem from "./modules/users/UsersItem";
import store from "./modules/app/store";

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<App />}>
            <Route index element={<About />} />
            <Route path="orders" element={<Orders />} />
            <Route path="users" element={<Users />} />
            <Route path="user/:userId" element={<UsersItem />} />
            <Route path="product/:productId" element={<ProductsItem />} />
            <Route path="about" element={<About />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </Provider>
  </React.StrictMode>
);
