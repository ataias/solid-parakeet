import "./App.css";

import { Box, useMediaQuery } from "@mui/material";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import { useAppDispatch, useAppSelector } from "./modules/app/hooks";
import { useEffect, useMemo } from "react";

import BaseBar from "./modules/app/BaseBar";
import { Outlet } from "react-router-dom";
import { fetchOrders } from "./modules/orders/ordersSlice";
import { fetchProducts } from "./modules/products/productsSlice";
import { fetchUsers } from "./modules/users/usersSlice";

function App() {
  const dispatch = useAppDispatch();
  const usersStatus = useAppSelector((state) => state.users.status);
  const productsStatus = useAppSelector((state) => state.products.status);

  const ordersStatus = useAppSelector((state) => state.orders.status);

  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");
  const theme = useMemo(
    () =>
      createTheme({
        palette: {
          mode: prefersDarkMode ? "dark" : "light",
        },
      }),
    [prefersDarkMode]
  );

  useEffect(() => {
    if (usersStatus === "idle") {
      dispatch(fetchUsers());
    }
  }, [usersStatus, dispatch]);

  useEffect(() => {
    if (productsStatus === "idle") {
      dispatch(fetchProducts());
    }
  }, [productsStatus, dispatch]);

  useEffect(() => {
    if (ordersStatus === "idle") {
      dispatch(fetchOrders());
    }
  }, [ordersStatus, dispatch]);

  useEffect(() => {
    // Set base colors for dark mode. We them to the root styles to make sure
    // the background an color of any elements outside of React root also take
    // effect. An alternative would be to make sure the React root always takes
    // full width and height and then just make sure it is set in our main div
    // component.
    const rootStyles = document.documentElement.style;
    rootStyles.setProperty(
      "--color-background-default",
      theme.palette.background.default
    );
    rootStyles.setProperty("--color-text-primary", theme.palette.text.primary);
  }, [prefersDarkMode]);

  return (
    <ThemeProvider theme={theme}>
      <Box component="div" className="App">
        <BaseBar />
        <Outlet />
      </Box>
    </ThemeProvider>
  );
}

export default App;
