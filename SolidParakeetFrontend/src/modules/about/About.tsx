import { Link, Typography } from "@mui/material";

import { Box } from "@mui/system";
import OpenInNewIcon from "@mui/icons-material/OpenInNew";

const About = () => {
  return (
    <Box maxWidth="800px" mx="auto">
      <Typography variant="h2">About this app</Typography>
      <Typography variant="body1" textAlign="justify" mb={2}>
        This a toy app to validate and reinforce my learnings of
        <ExternalLink name="React" url="https://reactjs.org/" />,
        <ExternalLink name="MUI" url="https://mui.com/" />,
        <ExternalLink name="Redux" url="https://redux.js.org/" /> and{" "}
        <ExternalLink name="TypeScript" url="https://www.typescriptlang.org" />.
        On top of it, I also used the
        <ExternalLink name="Dexie" url="https://dexie.org" />
        library to test it out too.
      </Typography>
      <Typography variant="body1" textAlign="justify" mb={2}>
        This app basically allows you to create users using a simple form and
        then create orders for the users. The products list is randomly
        generated and you can optionally randomly generate the users and orders
        as well. There is persistence using IndexedDB via{" "}
        <ExternalLink name="Dexie" url="https://dexie.org" />.
      </Typography>
      <Typography variant="body1" textAlign="justify">
        The code is open-source and you can find it on{" "}
        <ExternalLink
          name="Solid Parakeet's GitLab Repo"
          url="https://gitlab.com/ataias/solid-parakeet"
        />
        .
      </Typography>
      <Typography variant="h3" mt={2}>
        FAQ
      </Typography>
      <Typography variant="h4">Why name it Solid Parakeet?</Typography>
      <Typography variant="body1">It was randomly generated.</Typography>
    </Box>
  );
};

function ExternalLink({ name, url }: { name: string; url: string }) {
  return (
    <>
      {" "}
      <Link href={url} target="_blank" display="inline-flex">
        {name}
        <OpenInNewIcon />
      </Link>
    </>
  );
}

export default About;
