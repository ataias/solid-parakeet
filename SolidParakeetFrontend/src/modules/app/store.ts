import { configureStore } from "@reduxjs/toolkit";
import ordersSlice from "../orders/ordersSlice";
import productsSlice from "../products/productsSlice";
import usersSlice from "../users/usersSlice";

const store = configureStore({
  reducer: {
    users: usersSlice,
    orders: ordersSlice,
    products: productsSlice,
  },
});
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
