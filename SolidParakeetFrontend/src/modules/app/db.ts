import Dexie, { Table } from "dexie";

export interface User {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
}

export interface Product {
  id: string;
  name: string;
  description: string;
  price: number;
}

export interface Order {
  id: string;
  productId: string;
  userId: string;
  createdAt: string;
  deletedAt: string | null;
}

export class Database extends Dexie {
  // 'users' is added by dexie when declaring the stores()
  // We just tell the typing system this is the case
  users!: Table<User>;
  orders!: Table<Order>;
  products!: Table<Product>;

  constructor() {
    super("db_solid_parakeet");
    this.version(1).stores({
      users: "id, email, firstName, lastName",
      products: "id, name, description, price",
      orders: "id, productId, userId, createdAt, deletedAt",
    });
  }
}

export const db = new Database();
