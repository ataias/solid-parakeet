import {
  AppBar,
  Box,
  Container,
  IconButton,
  Link,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from "@mui/material";
import { Bird, Gitlab } from "mdi-material-ui";
import React, { Fragment, useState } from "react";

import MenuIcon from "@mui/icons-material/Menu";
import { Link as RouterLink } from "react-router-dom";

const pages = [
  { route: "/orders", label: "Orders" },
  { route: "/users", label: "Users" },
  { route: "/about", label: "About" },
];

const BaseBar = () => {
  const [anchorElNav, setAnchorElNav] = useState<null | HTMLElement>(null);

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) =>
    setAnchorElNav(event.currentTarget);

  const handleCloseNavMenu = () => setAnchorElNav(null);

  // !Hello
  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Logo screenVariant="medium" />

          {/* Hamburger menu for small screens */}
          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: "block", md: "none" },
              }}
            >
              {pages.map(({ label, route }) => (
                <MenuItem key={route} onClick={handleCloseNavMenu}>
                  <Link variant="button" to={route} component={RouterLink}>
                    <Typography textAlign="center">{label}</Typography>
                  </Link>
                </MenuItem>
              ))}
            </Menu>
          </Box>

          {/* Menu for medium screens and larger */}
          <Box
            sx={{ flexGrow: 1, display: { xs: "none", md: "flex" }, gap: 1 }}
          >
            {pages.map(({ label, route }) => (
              <Link
                variant="button"
                key={route}
                onClick={handleCloseNavMenu}
                component={RouterLink}
                to={route}
                sx={{ my: 2, color: "white", display: "block" }}
              >
                {label}
              </Link>
            ))}
          </Box>
          <Logo screenVariant="small" />
          <RepoLink />
        </Toolbar>
      </Container>
    </AppBar>
  );
};

function Logo({ screenVariant }: LogoProps) {
  const isSmall = screenVariant === "small";
  const display = isSmall
    ? { xs: "flex", md: "none" }
    : { xs: "none", md: "flex" };
  return (
    <Fragment>
      <Bird sx={{ display, mr: 1 }} />
      <Typography
        variant="h5"
        noWrap
        component={RouterLink}
        to="/"
        sx={[
          {
            mr: 2,
            display,
            flexGrow: 0,
            fontFamily: "monospace",
            fontWeight: 700,
            color: "inherit",
            textDecoration: "none",
          },
          isSmall && {
            flexGrow: 1,
          },
        ]}
      >
        Solid Parakeet
      </Typography>
    </Fragment>
  );
}

function RepoLink() {
  return (
    <Link
      href="https://gitlab.com/ataias/solid-parakeet"
      ml="auto"
      my="auto"
      target="_blank"
      color="#FFFFFF"
    >
      <Gitlab />
    </Link>
  );
}

interface LogoProps {
  screenVariant: "small" | "medium";
}

export default BaseBar;
