import { CircularProgress, Link } from "@mui/material";
import {
  DataGrid,
  GridColDef,
  GridRenderCellParams,
  GridToolbar,
  GridValueGetterParams,
} from "@mui/x-data-grid";

import { Link as RouterLink } from "react-router-dom";
import { User } from "../app/db";
import { selectAllUsers } from "./usersSlice";
import { useAppSelector } from "../app/hooks";

const columns: GridColDef[] = [
  { field: "id", hide: true },
  {
    field: "email",
    headerName: "Email",
    width: 200,
    renderCell: (params: GridRenderCellParams<string, User>) => (
      <Link to={`/user/${params.row.id}`} component={RouterLink}>
        {params.value}
      </Link>
    ),
  },
  { field: "firstName", headerName: "First name", width: 150 },
  { field: "lastName", headerName: "Last name", width: 150 },
  {
    field: "fullName",
    headerName: "Full name",
    description: "This column has a value getter and is not sortable.",
    sortable: false,
    width: 300,
    valueGetter: (params: GridValueGetterParams) =>
      `${params.row.firstName || ""} ${params.row.lastName || ""}`,
  },
];

export default function UsersList() {
  const users = useAppSelector(selectAllUsers);
  const usersStatus = useAppSelector((state) => state.users.status);
  const error = useAppSelector((state) => state.users.error);

  let content;
  if (usersStatus === "loading") {
    content = <CircularProgress />;
  } else if (usersStatus === "succeeded") {
    content = (
      <DataGrid
        rows={users}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
        components={{ Toolbar: GridToolbar }}
      />
    );
  } else if (usersStatus === "failed") {
    content = <div>{error}</div>;
  }

  return <div style={{ height: 400, width: "100%" }}>{content}</div>;
}
