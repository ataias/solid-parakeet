import { Button, Stack, TextField, Typography } from "@mui/material";
import React, { useRef } from "react";
import { validateEmail, validateRequired } from "../helpers/inputValidators";

import { addUser } from "./usersSlice";
import { useAppDispatch } from "../app/hooks";
import useInput from "../hooks/useInput";
import { v4 as uuidv4 } from "uuid";

export default function UsersAdd() {
  const { setValue: setEmail, ...email } = useInput("", [
    validateRequired,
    validateEmail,
  ]);
  const { setValue: setFirstName, ...firstName } = useInput("", [
    validateRequired,
  ]);
  const { setValue: setLastName, ...lastName } = useInput("", [
    validateRequired,
  ]);

  const isSubmitEnabled =
    email.value &&
    !email.error &&
    firstName.value &&
    !firstName.error &&
    lastName.value &&
    !lastName.error;

  const emailRef = useRef<HTMLDivElement>(null);

  const dispatch = useAppDispatch();
  const onSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
    if (!isSubmitEnabled) {
      return;
    }

    // TODO: add try catch and some loading state to know when user finished adding
    await dispatch(
      addUser({
        id: uuidv4(),
        email: email.value,
        firstName: firstName.value,
        lastName: lastName.value,
      })
    ).unwrap();
    setEmail("");
    setFirstName("");
    setLastName("");
    emailRef.current?.focus();
  };
  const onAddRandom = async () => {
    const response = await fetch("/api/user");
    const newUser = await response.json();
    dispatch(addUser(newUser));
  };

  return (
    <Stack
      component="form"
      spacing={1}
      onSubmit={onSubmit}
      border={2}
      padding={2}
      borderColor="secondary.light"
    >
      <Typography component="h2" variant="h4">
        Add New User
      </Typography>
      <TextField inputRef={emailRef} label="Email" {...email} />
      <TextField label="First Name" {...firstName} />
      <TextField label="Last Name" {...lastName} />
      <Stack spacing={1} justifyContent="center" direction="row">
        <Button type="submit" onClick={onSubmit} disabled={!isSubmitEnabled}>
          Submit
        </Button>
        <Button type="submit" onClick={onAddRandom}>
          Add Random
        </Button>
      </Stack>
    </Stack>
  );
}
