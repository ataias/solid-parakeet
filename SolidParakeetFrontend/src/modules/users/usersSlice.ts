import { User, db } from "../app/db";
import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
} from "@reduxjs/toolkit";

import { RootState } from "../app/store";

const usersAdapter = createEntityAdapter<User>();

const usersSlice = createSlice({
  name: "users",
  initialState: usersAdapter.getInitialState<AsyncStatus>({
    status: "idle",
    error: undefined,
  }),
  reducers: {},
  extraReducers(builder) {
    // Fetch users
    builder
      .addCase(fetchUsers.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchUsers.fulfilled, (state, action) => {
        state.status = "succeeded";
        usersAdapter.upsertMany(state, action.payload);
      })
      .addCase(fetchUsers.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      });

    // Add user
    builder.addCase(addUser.fulfilled, usersAdapter.addOne);
  },
});

export const fetchUsers = createAsyncThunk("users/fetchUsers", async () => {
  const users = await db.users.toArray();
  return users;
});

export const addUser = createAsyncThunk("users/addUser", async (user: User) => {
  await db.users.add(user);
  return user;
});

interface AsyncStatus {
  status: "idle" | "loading" | "succeeded" | "failed";
  error: string | undefined;
}

export default usersSlice.reducer;
export const { selectAll: selectAllUsers, selectById: selectUserById } =
  usersAdapter.getSelectors<RootState>((state) => state.users);
