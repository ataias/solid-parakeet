import { Box, Link, Typography } from "@mui/material";

import { Fragment } from "react";
import OrdersAdd from "../orders/OrdersAdd";
import OrdersList from "../orders/OrdersList";
import { selectOrdersByUser } from "../orders/ordersSlice";
import { selectUserById } from "./usersSlice";
import { useAppSelector } from "../app/hooks";
import { useParams } from "react-router-dom";

export default function UsersItem() {
  const params = useParams();
  const { userId = "" } = params;
  const user = useAppSelector((state) => selectUserById(state, userId));
  const orders = useAppSelector((state) => selectOrdersByUser(state, userId));

  let content;
  if (user) {
    content = (
      <Fragment>
        <Typography variant="h2">
          {user.firstName} {user.lastName}
        </Typography>
        <Link variant="body1" href={`mailto:${user.email}`}>
          {user.email}
        </Link>
        <Box my={2} mx={2}>
          <OrdersAdd initialUser={user} readonlyUser />
          <Typography variant="h3" my={2} component="h4">
            Previous Orders
          </Typography>
          <OrdersList orders={orders} />
        </Box>
      </Fragment>
    );
  } else {
    content = <Typography variant="h2">User Not Found</Typography>;
  }

  return <Box>{content}</Box>;
}
