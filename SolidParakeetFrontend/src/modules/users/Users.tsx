import { Box } from "@mui/material";
import UsersAdd from "./UsersAdd";
import UsersList from "./UsersList";

const Users = () => {
  return (
    <Box
      display="flex"
      flexDirection="column"
      gap={2}
      mt={2}
      maxWidth={{ sm: "100%", md: 1024 }}
      mx={{ sm: 2, md: "auto" }}
    >
      <UsersAdd />
      <UsersList />
    </Box>
  );
};
export default Users;
