import { CircularProgress, Link } from "@mui/material";
import {
  DataGrid,
  GridColDef,
  GridRenderCellParams,
  GridToolbar,
} from "@mui/x-data-grid";

import { Order } from "../app/db";
import { Link as RouterLink } from "react-router-dom";
import { formatDate } from "../helpers/time";
import { useAppSelector } from "../app/hooks";

const columns: GridColDef[] = [
  { field: "id", hide: true },
  { field: "userId", hide: true },
  { field: "productId", hide: true },
  {
    field: "productName",
    headerName: "Product",
    width: 200,
    renderCell: (params: GridRenderCellParams<string, RowOrder>) => (
      <Link to={`/product/${params.row.productId}`} component={RouterLink}>
        {params.value}
      </Link>
    ),
  },
  { field: "price", headerName: "Price", width: 150 },
  {
    field: "userFullName",
    headerName: "Client",
    width: 200,
    renderCell: (params: GridRenderCellParams<string, RowOrder>) => (
      <Link to={`/user/${params.row.userId}`} component={RouterLink}>
        {params.value}
      </Link>
    ),
  },
  {
    field: "orderDate",
    headerName: "Order Date",
    width: 250,
    renderCell: (params: GridRenderCellParams<string, RowOrder>) =>
      formatDate(params.row.orderDate),
  },
];

export default function OrdersList({ orders }: OrdersListProps) {
  const users = useAppSelector((state) => state.users.entities);
  const products = useAppSelector((state) => state.products.entities);

  const rowOrders = orders.map((order): RowOrder | null => {
    const product = products[order.productId];
    const user = users[order.userId];
    if (!product || !user) {
      return null;
    }
    return {
      id: order.id,
      productId: order.productId,
      userId: user.id,
      productName: product.name,
      price: `USD ${product.price}`,
      userFullName: `${user.firstName} ${user.lastName}`,
      orderDate: new Date(order.createdAt),
    };
  });
  const ordersStatus = useAppSelector((state) => state.orders.status);
  const error = useAppSelector((state) => state.orders.error);

  let content;
  if (ordersStatus === "loading") {
    content = <CircularProgress />;
  } else if (ordersStatus === "succeeded") {
    content = (
      <DataGrid
        rows={rowOrders.filter((r) => r)}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
        components={{ Toolbar: GridToolbar }}
      />
    );
  } else if (ordersStatus === "failed") {
    content = <div>{error}</div>;
  }

  return <div style={{ height: 400, width: "100%" }}>{content}</div>;
}

interface OrdersListProps {
  orders: Order[];
}

interface RowOrder {
  id: string;
  userId: string;
  productId: string;
  productName: string;
  price: string;
  userFullName: string;
  orderDate: Date;
}
