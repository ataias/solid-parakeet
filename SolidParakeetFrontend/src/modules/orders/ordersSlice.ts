import { Order, db } from "../app/db";
import {
  createAsyncThunk,
  createEntityAdapter,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";

import { RootState } from "../app/store";
import { v4 as uuidv4 } from "uuid";

const ordersAdapter = createEntityAdapter<Order>();

const ordersSlice = createSlice({
  name: "orders",
  initialState: ordersAdapter.getInitialState<AsyncStatus>({
    status: "idle",
    error: undefined,
  }),
  reducers: {},
  extraReducers(builder) {
    // Fetch orders
    builder
      .addCase(fetchOrders.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchOrders.fulfilled, (state, action) => {
        state.status = "succeeded";
        ordersAdapter.upsertMany(state, action.payload);
      })
      .addCase(fetchOrders.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      });

    // Add order
    builder.addCase(addOrder.fulfilled, ordersAdapter.addOne);
  },
});

export const fetchOrders = createAsyncThunk("orders/fetchOrders", async () => {
  const orders = await db.orders.toArray();
  return orders;
});

export const addOrder = createAsyncThunk(
  "orders/addOrder",
  async (partialOrder: Omit<Order, "id" | "createdAt" | "deletedAt">) => {
    const order = {
      id: uuidv4(),
      createdAt: new Date().toISOString(),
      deletedAt: null,
      ...partialOrder,
    };
    await db.orders.add(order);

    return order;
  }
);

interface AsyncStatus {
  status: "idle" | "loading" | "succeeded" | "failed";
  error: string | undefined;
}

export default ordersSlice.reducer;
export const { selectAll: selectAllOrders, selectById: selectOrderById } =
  ordersAdapter.getSelectors<RootState>((state) => state.orders);

export const selectOrdersByUser = createSelector(
  [selectAllOrders, (_state: RootState, userId: string) => userId],
  (orders, userId) => orders.filter((order: Order) => order.userId === userId)
);

export const selectOrdersByProduct = createSelector(
  [selectAllOrders, (_state: RootState, productId: string) => productId],
  (orders, productId) =>
    orders.filter((order: Order) => order.productId === productId)
);
