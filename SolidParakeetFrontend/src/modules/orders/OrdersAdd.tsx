import {
  Autocomplete,
  Box,
  Button,
  Link,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { Product, User } from "../app/db";
import React, { useState } from "react";
import { useAppDispatch, useAppSelector } from "../app/hooks";

import { Link as RouterLink } from "react-router-dom";
import { addOrder } from "./ordersSlice";
import { getRandomElement } from "../helpers/array";
import { selectAllProducts } from "../products/productsSlice";
import { selectAllUsers } from "../users/usersSlice";

export default function OrdersAdd({
  initialUser,
  readonlyUser,
}: OrdersAddProps) {
  const products = useAppSelector(selectAllProducts);
  const users = useAppSelector(selectAllUsers);

  const hasUsers = users.length > 0;

  const [product, setProduct] = useState<Product | null>(null);
  const [user, setUser] = useState<User | null>(initialUser || null);

  const isSubmitEnabled = product && user;

  const dispatch = useAppDispatch();
  const onSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
    if (!isSubmitEnabled) {
      return;
    }

    // TODO: add try catch and some loading state to know when user finished adding
    await dispatch(
      addOrder({
        productId: product.id,
        userId: user.id,
      })
    ).unwrap();
    setProduct(null);
  };
  const onAddRandom = async () => {
    const userId = readonlyUser && user ? user.id : getRandomElement(users).id;
    dispatch(
      addOrder({
        productId: getRandomElement(products).id,
        userId,
      })
    );
  };

  return (
    <Stack
      component="form"
      spacing={1}
      onSubmit={onSubmit}
      border={2}
      padding={2}
      borderColor="secondary.light"
    >
      <Typography component="h2" variant="h4">
        Add Order
      </Typography>
      <Autocomplete
        disablePortal
        options={products}
        getOptionLabel={(product) =>
          `${product.name} (USD ${product.price.toFixed(2)})`
        }
        renderOption={(props, product, index) => {
          const key = `listItem-${index}-${product.id}`;
          const label = `${product.name} (USD ${product.price.toFixed(2)})`;
          return (
            <Box component="li" {...props} key={key}>
              {label}
            </Box>
          );
        }}
        value={product}
        onChange={(_e, value) => setProduct(value)}
        renderInput={(params) => <TextField {...params} label="Product" />}
      />
      <Autocomplete
        disablePortal
        options={users}
        getOptionLabel={(user) => `${user.firstName} ${user.lastName}`}
        renderOption={(props, user, index) => {
          const key = `listItem-${index}-${user.id}`;
          const label = `${user.firstName} ${user.lastName}`;
          return (
            <Box component="li" {...props} key={key}>
              {label}
            </Box>
          );
        }}
        value={user}
        onChange={(_e, value) => setUser(value)}
        renderInput={(params) => <TextField {...params} label="User" />}
        readOnly={readonlyUser}
      />
      {!hasUsers && (
        <Typography variant="body1">
          To add an order, you should create at least one user first. Please go
          to the{" "}
          <Link to="/users" component={RouterLink}>
            Users page
          </Link>{" "}
          first
        </Typography>
      )}
      <Stack spacing={1} justifyContent="center" direction="row">
        <Button type="submit" onClick={onSubmit} disabled={!isSubmitEnabled}>
          Submit
        </Button>
        <Button type="submit" onClick={onAddRandom} disabled={!hasUsers}>
          Add Random
        </Button>
      </Stack>
    </Stack>
  );
}

export interface OrdersAddProps {
  initialUser?: User;
  readonlyUser?: boolean;
}
