import { Box } from "@mui/material";
import OrdersAdd from "./OrdersAdd";
import OrdersList from "./OrdersList";
import { selectAllOrders } from "./ordersSlice";
import { useAppSelector } from "../app/hooks";

const Orders = () => {
  const orders = useAppSelector(selectAllOrders);
  return (
    <Box
      display="flex"
      flexDirection="column"
      gap={2}
      mt={2}
      maxWidth={{ sm: "100%", md: 1024 }}
      mx={{ sm: 2, md: "auto" }}
    >
      <OrdersAdd />
      <OrdersList orders={orders} />
    </Box>
  );
};

export default Orders;
