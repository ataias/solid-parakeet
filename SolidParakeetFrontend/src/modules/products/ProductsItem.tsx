import { Box, Typography } from "@mui/material";

import { Fragment } from "react";
import OrdersList from "../orders/OrdersList";
import { selectOrdersByProduct } from "../orders/ordersSlice";
import { selectProductById } from "./productsSlice";
import { useAppSelector } from "../app/hooks";
import { useParams } from "react-router-dom";

export default function ProductsItem() {
  const params = useParams();
  const { productId = "" } = params;
  const product = useAppSelector((state) =>
    selectProductById(state, productId)
  );

  const orders = useAppSelector((state) =>
    selectOrdersByProduct(state, productId)
  );

  let content;
  if (product) {
    content = (
      <Fragment>
        <Typography variant="h2">{product.name}</Typography>
        <Typography textAlign="justify" variant="body1">
          {product.description}
        </Typography>
        <Typography variant="h3">Price</Typography>
        <Typography variant="body1">US$ {product.price}</Typography>
        <Typography variant="h3">Product Orders</Typography>
        <OrdersList orders={orders} />
      </Fragment>
    );
  } else {
    content = <Typography variant="h2">Product Not Found</Typography>;
  }

  return <Box mx={4}>{content}</Box>;
}
