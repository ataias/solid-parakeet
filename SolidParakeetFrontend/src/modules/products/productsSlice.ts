import { Product, db } from "../app/db";
import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
} from "@reduxjs/toolkit";

import { RootState } from "../app/store";
import { v4 as uuidv4 } from "uuid";

const productsAdapter = createEntityAdapter<Product>();

const productsSlice = createSlice({
  name: "products",
  initialState: productsAdapter.getInitialState<AsyncStatus>({
    status: "idle",
    error: undefined,
  }),
  reducers: {},
  extraReducers(builder) {
    // Fetch products
    builder
      .addCase(fetchProducts.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchProducts.fulfilled, (state, action) => {
        state.status = "succeeded";
        productsAdapter.upsertMany(state, action.payload);
      })
      .addCase(fetchProducts.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      });

    // Add product
    builder.addCase(addProduct.fulfilled, productsAdapter.addOne);
  },
});

export const fetchProducts = createAsyncThunk(
  "products/fetchProducts",
  async () => {
    let products = await db.products.toArray();
    if (products.length === 0) {
      const response = await fetch("/api/products?quantity=100");
      const newProducts = await response.json();
      await db.products.bulkAdd(newProducts);
      products = await db.products.toArray();
    }

    return products;
  }
);

export const addProduct = createAsyncThunk(
  "products/addProduct",
  async (partialProduct: Omit<Product, "id">) => {
    const product = {
      id: uuidv4(),
      ...partialProduct,
    };
    await db.products.add(product);

    return product;
  }
);

interface AsyncStatus {
  status: "idle" | "loading" | "succeeded" | "failed";
  error: string | undefined;
}

export default productsSlice.reducer;
export const { selectAll: selectAllProducts, selectById: selectProductById } =
  productsAdapter.getSelectors<RootState>((state) => state.products);
