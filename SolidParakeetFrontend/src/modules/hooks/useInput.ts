import React, { useState } from "react";

export default function useInput(
  initialValue: string,
  validators?: ((value: string) => string)[]
) {
  const [value, setValue] = useState<string>(initialValue);
  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };

  const [errorText, setErrorText] = useState<string>("");
  const onBlur = () => {
    if (validators) {
      setErrorText(
        validators
          .map((validate) => validate(value))
          .filter((message) => message)
          .join(" ")
      );
    }
  };
  return {
    value,
    setValue,
    onChange,
    onBlur,
    error: errorText.length > 0,
    helperText: errorText,
  } as const;
}
