import { isEmailValid } from "./email";
export function validateEmail(value: string): string {
  if (!isEmailValid(value)) {
    return "Invalid e-mail.";
  }
  return "";
}

export function validateRequired(value: string): string {
  if (value.length === 0) {
    return "Required.";
  }
  return "";
}
