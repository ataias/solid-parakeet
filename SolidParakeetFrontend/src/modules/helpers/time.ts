export function sleep(milliseconds: number) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}

export function formatDate(date: Date): string {
  const language = window.navigator.language;
  const formatter = new Intl.DateTimeFormat(language, {
    dateStyle: "long",
    timeStyle: "medium",
  });
  return formatter.format(date);
}
