import checker from "vite-plugin-checker";
import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    checker({
      typescript: true,
      eslint: {
        lintCommand: 'eslint "./src/**/*.{ts,tsx}"', // for example, lint .ts & .tsx
      },
    }),
  ],
  server: {
    proxy: {
      "/api": {
        // TODO: this could by default use an environment variable whose default is the deployed api
        target: "https://solid-parakeet.case.dev.br",
        changeOrigin: true,
        // If you are using the production server, a rewrite is already done;
        // you may only need the next line only if you are running the server
        // locally
        //
        // rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },
  build: {
    sourcemap: true,
  },
});
